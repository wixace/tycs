# 天缘传说

## 介绍

**开发环境**:MacOS 10.15.4,Xcode 11.5,Cocos2d-x 3.16,Cocos Creator 2.3.4

目前已支持appType 1,2,3



## TODO

![输入图片说明](https://images.gitee.com/uploads/images/2020/0615/070854_f335e833_4790212.png "屏幕截图.png")

* 由于cocos2d-x 3.16版本还在使用UIWebView，因此游戏方需要修改引擎代码，使其替换为WkWebView [具体详情](https://forum.cocos.org/t/uiwebview-wkwebview/93029)

* 还未添加垃圾代码,需要游戏方根据情况添加

  

## 使用说明

`cocos run . -p ios -m release`

