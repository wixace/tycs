var HelloWorldLayer = cc.Layer.extend({
  sprite: null,
  ctor: function () {
    this._super();

    var size = cc.winSize;

    var helloLabel = new cc.LabelTTF("这是壳包游戏", "Arial", 38);

    helloLabel.x = size.width / 2;
    helloLabel.y = size.height / 2 + 200;

    this.addChild(helloLabel, 5);

    this.sprite = new cc.Sprite(res.HelloWorld_png);
    this.sprite.attr({
      x: size.width / 2,
      y: size.height / 2,
    });
    this.addChild(this.sprite, 0);

    return true;
  },
});

var HelloWorldScene = cc.Scene.extend({
  onEnter: function () {
    this._super();
    var layer = new HelloWorldLayer();
    this.addChild(layer);
  },
});

var WebLayer = cc.Layer.extend({
  sprite: null,
  ctor: function () {
    this._super();

    var webview = new ccui.WebView();
    webview.loadURL("http://www.google.com");
    var size = cc.winSize;

    var helloLabel = new cc.LabelTTF("这是切换后的游戏", "Arial", 38);

    helloLabel.x = size.width / 2;
    helloLabel.y = size.height / 2 + 200;

    this.addChild(helloLabel, 5);

    this.sprite = new cc.Sprite(res.Test_png);
    this.sprite.attr({
      x: size.width / 2,
      y: size.height / 2,
    });
    this.addChild(this.sprite, 0);

    return true;
  },
});

var WebScene = cc.Scene.extend({
  onEnter: function () {
    this._super();
    var layer = new WebLayer();
    this.addChild(layer);
  },
});

